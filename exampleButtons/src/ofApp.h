#pragma once

#include "ofMain.h"
#include "module.h"
#include "ofxGui.h"

using namespace module;

class ofApp : public ofBaseApp{
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    
    ButtonImage btnImage{"Mic Button", "mic_2x.png"};
    ButtonText btnText{"Text Button", "PUSH"};

    ofxPanel gui;
};
