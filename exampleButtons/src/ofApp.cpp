#include "ofApp.h"

void ofApp::setup(){
    
    gui.setup("settings", "settings.json");
    gui.add(btnImage.prm);
    gui.add(btnText.prm);
    
    gui.loadFromFile("settings.json");
    
    FontManager::setup();
}

void ofApp::update(){

}

void ofApp::draw(){

    ofPushMatrix();
    {
        ofSetColor(0);
        ofDrawLine(-50, 0, 50, 0);
        ofDrawLine(0, -50, 0, 50);
        
        btnImage.draw();
        btnText.draw();
    }
    ofPopMatrix();
    
    gui.draw();
}

void ofApp::keyPressed(int key){

    switch(key){
            
        case 'e':
            btnImage.enable();
            btnText.enable();
            break;

        case 'd':
            btnImage.disable();
            btnText.disable();
            break;
    }
}
