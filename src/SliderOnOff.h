//
//  SliderOnOff
//
#pragma once

#include "ofMain.h"
#include "module.h"
#include "ButtonBase.h"

//#define USE_SVG
#ifdef USE_SVG
#include "ofxSvg.h"
#endif

using namespace glm;

namespace module{
    
    class SliderOnOff{
        
    public:
        SliderOnOff(){
            
            pos = vec2(-buttonDist,0);
            prevPos = pos;

#ifdef USE_SVG
            filesystem::path p = "frameOnOff.svg";
#else
            filesystem::path p = "frame.png";
#endif
            sliderFrame.load(p.string());
        }
        
        void setup(){
            
        }
        
        void mousePressed(ofMouseEventArgs &e){
            bHit = hitTest(e.x, e.y);
            prevPos = e - vec2(ofGetWidth()/2, ofGetHeight()/2);
        }
        
        void mouseDragged(ofMouseEventArgs &e){
            if(!bHit) return;
            float mx = e.x - ofGetWidth()/2;
            pos.x = ofClamp(mx, -buttonDist, buttonDist);
            bOn = pos.x > 0;
        }
        
        void mouseReleased(ofMouseEventArgs &e){
            if(bHit){
                // click or drag?
                vec2 mPos = e - vec2(ofGetWidth()/2, ofGetHeight()/2);
                float dist = glm::distance(mPos, prevPos);
                bDrag = dist > 10;
                
                if(!bDrag){
                    bOn = !bOn;
                }else{
                    bOn = pos.x > 0;
                }
            }
            bHit = false;
        }
        
        bool hitTest(int tx, int ty){
			tx -= ofGetWidth() / 2;
			ty -= ofGetHeight() / 2;

			if (bAllowDrag) {
				float dist = sqrt((tx - pos.x - center->x)*(tx - pos.x - center->x) + (ty - pos.y - center->y)*(ty - pos.y - center->y));
				return dist <= buttonSize;
			}
			else {
				float padding = 20; //add extra padding for touch surface
				float w = buttonSize * 4 + buttonDist + padding;
				float h = buttonSize * 2 + padding;
				return ((tx > center->x - w / 2) && (tx < center->x + w) && (ty > center->y - h / 2) && (ty < center->y + h));
			}
			
		}
        
        void update(){            
            pos.x = bOn ? buttonDist : -buttonDist;
        }
        
        void draw(float fade) {
            
            ofPushMatrix();
            ofTranslate(center);
            
            // frame
            ofSetColor(255, 255*fade);
            
#ifdef USE_SVG
            sliderFrame.draw();
#else
            ofSetRectMode(OF_RECTMODE_CENTER);
            sliderFrame.draw(0,0);
            ofSetRectMode(OF_RECTMODE_CORNER);
#endif

            // button
            ofFill();
            ofSetColor(255, 255*fade);
            ofDrawCircle(pos, buttonSize);
            
            // draw text inside of Button
            int fontSize = 14;
            //ofRectangle box = module::FontManager::GothamBold[fontSize].getStringBoundingBox(label[bOn], 0, 0);
            ofSetColor(0, 255*fade);
            //module::FontManager::GothamBold[fontSize].setLetterSpacing(1.3);
            //module::FontManager::GothamBold[fontSize].drawString(label[bOn], pos.x-box.width/2.0f+2, pos.y+box.height/2.0f);
            
            ofPopMatrix();
        }
        
        void debugDraw(){}
        
        void enable(){
            ofAddListener(ofEvents().mousePressed, this, &SliderOnOff::mousePressed);
			ofAddListener(ofEvents().mouseReleased, this, &SliderOnOff::mouseReleased);
			if (bAllowDrag) {
				ofAddListener(ofEvents().mouseDragged, this, &SliderOnOff::mouseDragged);
			}
        }
        
        void disable(){
            ofRemoveListener(ofEvents().mousePressed, this, &SliderOnOff::mousePressed);
            ofRemoveListener(ofEvents().mouseReleased, this, &SliderOnOff::mouseReleased);
			if (bAllowDrag) {
				ofRemoveListener(ofEvents().mouseDragged, this, &SliderOnOff::mouseDragged);
			}
        }
        
        ofParameter<bool> bOn{false};
        
    private:
        vec2 pos;
        vec2 prevPos;
        
		float buttonDist{ 40 };
		bool bHit{ false };
		bool bDrag{ false };
		bool bAllowDrag{ false }; //enable/disable drag
        
        ofParameter<vec2> center{"center", vec2(0,0),-vec2(1920/2,1080/2),vec2(1920/2,1080/2)};
        ofParameter<float>  buttonSize{"buttonSize", 50, 10, 200};

#ifdef USE_SVG
        ofxSVG sliderFrame;
#else
        ofImage sliderFrame;
#endif
        vector<string> label{ "OFF", "ON" };
        
    public:
        ofParameterGroup prm{"SliderOnOff", center, buttonSize};
        
    };
}
