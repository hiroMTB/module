#include "ofMain.h"
#include "Util.h"

namespace module{
    
    void Util::drawLine( glm::vec2 p1, glm::vec2 p2, float thickness){
        
        ofFill();
        glm::vec2 xAxis(1,0);
        glm::vec2 dir = p2 - p1;
        float dist = glm::length(dir);
        glm::vec2 cen = p1 + dir*0.5;
        glm::vec2 ndir = glm::normalize(dir);
        float angle = glm::orientedAngle(xAxis, ndir);
        ofPushMatrix();
        ofTranslate(cen);
        ofRotateZRad(angle);

        ofSetRectMode(OF_RECTMODE_CENTER);
        ofDrawRectangle(0, 0, dist, thickness);
        ofSetRectMode(OF_RECTMODE_CORNER);

        ofPopMatrix();
    }
    
    void Util::drawLine( float x1, float y1, float x2, float y2, float thickness){
        drawLine( glm::vec2(x1, y1), glm::vec2(x2, y2), thickness);
    }
    
    void Util::drawCircle( float x, float y, float rad, float thickness){
        
        if(ofGetStyle().color.a>0 && 0<rad){
            ofPath circle;
            circle.setCircleResolution(120);
            circle.setColor(ofGetStyle().color);
            circle.arc(x,y,rad+thickness/2,rad+thickness/2, 0,360);
            circle.arc(x,y,rad-thickness/2,rad-thickness/2, 0,360);
            circle.draw();
        }
    }
    
    void Util::drawArc( float x, float y, float rad, float thickness, float startAngle, float endAngle){
        
        ofNoFill();
        
        if(ofGetStyle().color.a>0 && 0<rad){
            ofPath arc;
            arc.setColor(ofGetStyle().color);
            arc.setCircleResolution(120);
            
            ofPoint outerStartPoint;
            outerStartPoint.x = x + (rad+thickness/2)*cos(ofDegToRad(startAngle));
            outerStartPoint.y = y + (rad+thickness/2)*sin(ofDegToRad(startAngle));
            
            ofPoint innerEndPoint;
            innerEndPoint.x = x + (rad-thickness/2)*cos(ofDegToRad(endAngle));
            innerEndPoint.y = y + (rad-thickness/2)*sin(ofDegToRad(endAngle));
            
            arc.arc(ofPoint(x,y), rad+thickness/2,rad+thickness/2, startAngle, endAngle, true);
            arc.lineTo(innerEndPoint);
            arc.arc( ofPoint(x,y),rad-thickness/2,rad-thickness/2, endAngle, startAngle, false);
            //arc.lineTo(outerStartPoint);
            
            arc.close();
            
            arc.draw();
        }
    }
    
    filesystem::path Util::getCommonFolder(){
        return filesystem::path("../../../common/");
    }
    
    string Util::replaceAll(string str, const string& from, const string& to) {
        size_t start_pos = 0;
        while((start_pos = str.find(from, start_pos)) != string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
        }
        return str;
    }

    int Util::findString(string str, const string& key) {
        int count = 0;
        size_t start_pos = 0;
        string temp = str;
        while((start_pos = str.find(key, start_pos)) != string::npos) {
            str.replace(start_pos, key.length(), "");
            start_pos += key.length();
            count++;
        }
        return count;
    }
    
    void Util::getLoadResult(bool ok, string module, filesystem::path path){
        
        if(ok){
            if(ofGetLogLevel()>=OF_LOG_NOTICE){
                cout << left << setw (10) << module << ": OK    : Load " << path.filename().string() << endl;
            }
        }else{
            if(ofGetLogLevel()>=OF_LOG_ERROR){
                cout << left << setw (10) << module << ": ERROR : Can not load " << path.string() << endl;
            }
        }
    }
}
