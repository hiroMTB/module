//
//  TextBox.h
//

#include "ofMain.h"
#include "module.h"

using namespace glm;

namespace module{
    
    /*
    
        Future develeopment plan
     
        - try to follow Adobe AI's text box system
            - 1. Specify Box layout
                 position, size
                 alignment by ofAlighHorz and ofAlighVert

            - 2. Specify Text layout inside of box
                 alignment by ofAlighHorz (Adobe AI does not have text vertical alignment, I guess)

            - 3. All text should be fit inside of box
     
            - 4. No fit text should have another class (like TextLine class maybe)
     
        - Example usage idea
            TextBox tb{"myTextBox", ofRectangle(0,0,200,200)};
            tb.setBoxShape(200, 200, 100, 100);             // TextBox at xy(200, 200), size(100,100), box center is (250,250)
            tb.setBoxAlignHorz(OF_ALIGN_HORZ_CENTER);       // align horizontal center, now box center is (200, 250)
            tb.setBoxAlignVert(OF_ALIGN_VERT_CENTER);       // align vertical centerm, now box  center is (200, 200)
            tb.setTextHorzVert(OF_ALIGN_HORZ_LEFT);         // align text to left of box, text start from 150, 150 (top corner)
            tb.setTextAlignVert(OF_ALIGN_VERT_CENTER);      // align text to vertical center (Illustrator does not have this feature, we can skip)
     
        - see rectangleAlignmentAndScallingExample for alighment usage
    */
    class TextBox {
        
    public:
        
        TextBox(){
            prm.setName("text");
        }
        
        TextBox(string name, string _text){
            prm.setName(name);
            text = _text;
        }
        
		void draw(module::Font & font) {

			float lh = lineHeight; //(lineHeight / textSize) * maxTextSize;

			string t = Util::replaceAll((bFrench ? text_fr : text), "\\n", "\n");

			font[textSize].setLetterSpacing(letterSpacing);
			font[textSize].setLineHeight(lh);
			ofRectangle rAll = font[textSize].getStringBoundingBox(t, 0, 0);
			float toTopLeft = font[textSize].getAscenderHeight() + font[textSize].getDescenderHeight();		// Dont know why this works ok...

			ofPushMatrix();
			ofTranslate(textPos->x, textPos->y);

			if (!alignBaseline){
				// align top left
				string first = t.substr(0, 1);
				ofTranslate(0, toTopLeft);
			}

            ofSetColor(color);
			
            if(centering){
                vector<string> lines = ofSplitString(t, "\n");

                int yBuf = 0;
                for (int i=0; i<lines.size(); i++){
                    ofRectangle r = font[textSize].getStringBoundingBox(lines[i], 0, 0);
                    float w = r.width;
                    float h = r.height;
                    if(!fit){
						if (!alignBaseline) {
							font[textSize].drawString(lines[i], -w / 2, yBuf - rAll.height*0.5);
						}
						else {
							font[textSize].drawString(lines[i], -w / 2, yBuf);
						}
                    }else{
                        if(yBuf<=fitHeight){
							if (!alignBaseline) {
								font[textSize].drawStringFit(lines[i], -fitWidth / 2, yBuf - fitHeight / 2, fitWidth, 0);
							}
							else {
								font[textSize].drawStringFit(lines[i], -fitWidth / 2, yBuf, fitWidth, 0);
							}
                        }
                    }
					yBuf += lh;
                }
            }else{
                if(!fit){
                    font[textSize].drawString(t, 0, 0);
                }else{
					if (!alignBaseline) {
						font[textSize].drawWrappedString(t, 0, 0, fitWidth, fitHeight);
					}
					else {
						font[textSize].drawWrappedString(t, 0, 0, fitWidth, fitHeight);
					}
                }
            }
            ofPopMatrix();
        }
        
        void debugDraw(module::Font & font) {

			float lh = lineHeight; //(lineHeight / textSize) * maxTextSiz;

            string t = Util::replaceAll( (bFrench ? text_fr : text), "\\n", "\n");

            font[textSize].setLetterSpacing(letterSpacing);
            font[textSize].setLineHeight(lh);

            ofPushStyle();
            ofSetLineWidth(1);
            ofPushMatrix();
            ofTranslate(textPos->x, textPos->y);
			
            ofPushMatrix();
            {
				string first = t.substr(0, 1);
				float toTopLeft = font[textSize].getAscenderHeight() + font[textSize].getDescenderHeight();

				if (!alignBaseline) {
					// align top left
					ofTranslate(0, toTopLeft);
				}

                ofNoFill();
                ofSetColor(0,0,255);
                ofRectangle r = font[textSize].getStringBoundingBox(t, 0, 0);
                if (centering) {
                    if(!fit){
						if (!alignBaseline) {
							ofSetRectMode(OF_RECTMODE_CENTER);
							ofDrawRectangle(0, -toTopLeft, r.width, r.height);
							ofSetRectMode(OF_RECTMODE_CORNER);
						}
						else {
							ofDrawRectangle(-r.width/2, -font[textSize].getAscenderHeight(), r.width, r.height);
						}
                    }else{
						if (!alignBaseline) {
							ofSetRectMode(OF_RECTMODE_CENTER);
							ofDrawRectangle(0, -toTopLeft, fitWidth, fitHeight);
							ofSetRectMode(OF_RECTMODE_CORNER);
						}
						else {
							ofDrawRectangle(-fitWidth/2, -font[textSize].getAscenderHeight(), fitWidth, fitHeight);
						}
                    }
                }else {
                    if(!fit){
                        ofDrawRectangle(0, -toTopLeft, r.width, r.height);
                    }else{
                        ofDrawRectangle(0, -toTopLeft, fitWidth, fitHeight);
                    }
                }
            }
            ofPopMatrix();
            
            // origin
            ofDrawAxis(100);
            ofFill();
            ofSetColor(255,0,0);
            ofDrawCircle(0, 0, 5);
            
            ofPopMatrix();
            ofPopStyle();
        }
		bool bFrench{ false };
        ofParameter<string> text{"text", "test"};
        ofParameter<string> text_fr{"text FR", "test FR"};
        ofParameter<int> textSize{"size", 30, 10, 300};
        ofParameter<vec2> textPos{"position", vec2(0,0), -vec2(1920, 1080), vec2(1920, 1080)};
        ofParameter<bool> centering{"centering", true};
        ofParameter<bool> alignBaseline{"align baseline", false};
        ofParameter<float> letterSpacing{"letterSpacing", 1, 0, 10};
        ofParameter<float> lineHeight{"line height", 0, 10, 200};

        ofParameter<bool> fit{"fit", false};
        ofParameter<float> fitWidth{"fitWidth", 100, 1, 1920};
        ofParameter<float> fitHeight{"fitHeight", 100, 1, 1080};
        ofParameter<ofColor> color{"color", ofColor(100, 100, 100)};
        
        ofParameterGroup prm{"Text", text, text_fr, textSize, textPos, centering, alignBaseline, letterSpacing, lineHeight, fit, fitWidth, fitHeight, color};
    };
    
}
