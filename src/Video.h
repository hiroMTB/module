//
//  Video.h
//

#pragma once

namespace module{
    
    class Video{
        
    public:

        Video(){
            path.addListener(this, &Video::pathChanged);
            loop.addListener(this, &Video::loopChanged);
            vidFrame.setSerializable(false);
            fps.setSerializable(false);
        }
        
        void load(){
            if(loop) vid.setLoopState(OF_LOOP_NORMAL);
            else vid.setLoopState(OF_LOOP_NONE);
            bool ok = vid.load(path.get());
            if(ok){
                float totalFrame = vid.getTotalNumFrames();
                float totalSec = vid.getDuration();
                fps = round(totalFrame/totalSec);
                vidFrame.setMax(totalFrame);
                ofLogNotice() << "Load Video : " << fps << " fps, " << path.get();
            }else{
                ofLogError() << "Can not load Video : " << path.get();
            }
            //assert(ok);
        }

        void draw(float fade=1){
            if (show){
                ofSetColor(255, 255*fade);
                ofSetRectMode(OF_RECTMODE_CENTER);
                vid.draw(pos);
                ofSetRectMode(OF_RECTMODE_CORNER);
            }
        }
        
        bool update(){
            vid.update();
            vidFrame = vid.getCurrentFrame();
            return vid.getIsMovieDone();
        }
        
        bool getIsMovieDone(){
            return vid.getIsMovieDone();
        }
        
        void start(){
            vid.setPosition(0);
            vid.play();
        }
        
        void stop(){
            vid.stop();
        }
        
        int getCurrentFrame() const {
            return vid.getCurrentFrame();
        }

        float getCurrentSec() const{
            return vid.getCurrentFrame() / fps;
        }
        
        float getFps() const{
            return fps;
        }
        
    private:
        void pathChanged(string & p){
            load();
        }
        
        void loopChanged(bool & b){
            if(loop) vid.setLoopState(OF_LOOP_NORMAL);
            else vid.setLoopState(OF_LOOP_NONE);
        }

    private:
        ofParameter<string> path{ "path", "n.a." };
        ofParameter<float> fps{ "fps", 0, 0, 120};
        ofParameter<vec2> pos{ "position", vec2(0,0), -vec2(1920/2, 1080/2), vec2(1920/2, 1080/2) };
        ofParameter<bool> show{ "show", true };
        ofParameter<bool> loop{ "loop", false };
        ofParameter<int> vidFrame{ "vidFrame", 0, 0, 1000};
        
    public:
        ofVideoPlayer vid;
        ofParameterGroup prm{ "video", pos, path, fps, show, loop, vidFrame};
    };
}
