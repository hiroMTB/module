//
//  Button.h
//
#pragma once
#include "module.h"
#include "ButtonBase.h"

namespace module {
	class ButtonText : public module::ButtonBase {

	public:

        ButtonText(string name, string _text){
            tb.text = _text;
            prm.add(tb.prm);
            prm.add(boxColor);
			prm.add(borderPrm);
            prm.setName(name);
            prm.add(width);
            prm.add(height);
        }
        
        bool hitTest(int tx, int ty) const override{
            const vec2 &p = pos.get();
            return ((tx > p.x - width/2) && (tx < p.x + width/2) && (ty > p.y - height/2) && (ty < p.y + height/2));
        }
        
        void draw() override{}
        
        void draw(module::Font & font) {

            ofPushMatrix();
            ofTranslate(pos.get());

			ofNoFill();
			if (bIsEnable) {
				ofSetColor(borderColor);
			}
			else {
				ofSetColor(borderColor->r * 0.5, borderColor->g * 0.5, borderColor->b * 0.5, borderColor->a);
			}
			ofSetLineWidth(boarderWidth);
			ofSetRectMode(OF_RECTMODE_CENTER);
			ofDrawRectRounded(0, 0, width, height, cornerRadius);
			ofSetRectMode(OF_RECTMODE_CORNER);
			
            
            float a = isMousePressed() ?  100/255.0 : 1.0;
			ofFill();

			if (bIsEnable) {
				ofSetColor(boxColor, boxColor.get().a * a);
			}
			else {
				ofSetColor(boxColor->r * 0.5, boxColor->g * 0.5, boxColor->b * 0.5, boxColor->a);
			}
            ofSetRectMode(OF_RECTMODE_CENTER);
            ofDrawRectangle(0, 0, width, height);
            ofSetRectMode(OF_RECTMODE_CORNER);
        
            // draw text inside of Button
            tb.draw(font);
            ofPopMatrix();
		}
        
        void debugDraw(module::Font & font) {
            ofPushMatrix();
            ofTranslate(pos.get());
            tb.debugDraw(font);
            ofPopMatrix();
        }
        
        ofParameter<float> width{ "width", 0, 0, 1920 };
        ofParameter<float> height{ "height", 0,0,  1080 };
        ofParameter<ofColor> boxColor{"boxColor", ofColor(100, 100, 100)};

		ofParameter<ofColor> borderColor{ "color", ofColor(255,255), ofColor(0,0), ofColor(255,255) };
		ofParameter<int> boarderWidth{ "width", 4, 0, 20 };
		ofParameter<int> cornerRadius{ "cornerRadius", 4, 0, 100 };
		ofParameterGroup borderPrm{ "border", borderColor, boarderWidth, cornerRadius };

        module::TextBox tb;
	};
    
    class ButtonTextCircle : public module::ButtonBase {
        
    public:
        
        ButtonTextCircle(string name, string _text){
            tb.text = _text;
            prm.add(tb.prm);
            prm.add(boxColor);
            prm.setName(name);
            prm.add(radius);
        }
        

        bool hitTest(int tx, int ty) const override{
            const vec2 &p = pos.get();
            return glm::distance(p, glm::vec2(tx, ty)) <= radius;
        }
        
        void draw() override{}
        
        void draw(module::Font & font) {
            
            ofPushMatrix();
            ofTranslate(pos.get());
            
            float a = isMousePressed() ?  100/255.0 : 1.0;

            ofNoFill();
            ofSetColor(boxColor, boxColor.get().a*a);
            ofDrawCircle(0, 0, radius);
            
            // draw text inside of Button
            tb.draw(font);
            ofPopMatrix();
        }
        
        void debugDraw(module::Font & font) {
            ofPushMatrix();
            ofTranslate(pos.get());
            tb.debugDraw(font);
            ofPopMatrix();
        }
        
        ofParameter<float> radius{ "radius", 100, 0, 1920 };
        ofParameter<ofColor> boxColor{"boxColor", ofColor(100, 100, 100)};
        module::TextBox tb;
    };

	class SwitchTextCircle : public module::ButtonBase{

	public:

		SwitchTextCircle(string name, string _text) {
			tb.text = _text;
			prm.add(tb.prm);
			prm.add(boxColor);
			prm.add(bgColor);
			prm.add(textColorOff);
			prm.add(textColorOn);
			prm.setName(name);
			prm.add(radius);
		}


		bool hitTest(int tx, int ty) const override {
			const vec2 &p = pos.get();
			return glm::distance(p, glm::vec2(tx, ty)) <= radius;
		}

        void turnOn() override {
            bIsOn = true;
            tb.color = textColorOn;
        }
        
		void turnOff() override {
			bIsOn = false;
			tb.color = textColorOff;
		}

		void mouseMoved(ofMouseEventArgs &e) override {
			if (!bIsEnable) return;

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void mousePressed(ofMouseEventArgs &e) override {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
				/*if (!bIsOn) {
					turnOn();
				}
				else {
					turnOff();
				}*/
			}
			else {
				bIsMousePressed = false;
			}

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void mouseDragged(ofMouseEventArgs &e) override {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
			}
			else {
				bIsMousePressed = false;
			}

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void mouseReleased(ofMouseEventArgs &e) override {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				if (!bIsOn) {
					turnOn();
					ofNotifyEvent(buttonPressed, *this);
				}
				else {
					turnOff();
				}
			}
			bIsMousePressed = false;

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void draw() override {}

		void draw(module::Font & font) {

			ofPushMatrix();
			ofTranslate(pos.get());

			ofFill();
			if (bIsEnable) {
				ofSetColor(bgColor);
			}
			else {
				ofSetColor(bgColor->r*0.5, bgColor->g * 0.5, bgColor->b * 0.5, bgColor->a);
			}
			ofDrawCircle(0, 0, radius);

			float a = bIsOn ? 100 / 255.0 : 1.0;
			
			if (bIsOn) {
				ofFill();
			}
			else {
				ofNoFill();
			}

			if (bIsEnable) {
				ofSetColor(boxColor);
			}
			else {
				ofSetColor(boxColor->r * 0.5, boxColor->g * 0.5, boxColor->b * 0.5, boxColor->a);
			}
			ofDrawCircle(0, 0, radius);

			// draw text inside of Button
			tb.draw(font);
			ofPopMatrix();
		}

		void debugDraw(module::Font & font) {
			ofPushMatrix();
			ofTranslate(pos.get());
			tb.debugDraw(font);
			ofPopMatrix();
		}

		ofParameter<float> radius{ "radius", 100, 0, 1920 };
		ofParameter<ofColor> boxColor{ "boxColor", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		ofParameter<ofColor> bgColor{ "bgColor", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		ofParameter<ofColor> textColorOff{ "textColorOff", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		ofParameter<ofColor> textColorOn{ "textColorOn", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		module::TextBox tb;


	private:
		
	};

}
