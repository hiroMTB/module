//
//  ToggleText.h
//

#pragma once
#include "module.h"
#include "ButtonBase.h"

namespace module {
	class Checkbox : public module::ButtonBase{

	public:

		Checkbox(string name, string _text) {
			tb.text = _text;
			prm.add(tb.prm);
			prm.add(boxColor);
			prm.add(borderPrm);
			prm.add(width);
			prm.add(height);

			prm.setName(name);
		}


		bool hitTest(int tx, int ty) const override {
			const vec2& p = pos.get();
			return ((tx > p.x - width / 2) && (tx < p.x + width / 2) && (ty > p.y - height / 2) && (ty < p.y + height / 2));
		}

		void turnOn() override {
			bIsOn = true;
		}

		void turnOff() override {
			bIsOn = false;
		}

		void mouseMoved(ofMouseEventArgs & e) override {
			if (!bIsEnable) return;

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void mousePressed(ofMouseEventArgs & e) override {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
				/*if (!bIsOn) {
					turnOn();
				}
				else {
					turnOff();
				}*/
			}
			else {
				bIsMousePressed = false;
			}

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void mouseDragged(ofMouseEventArgs & e) override {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
			}
			else {
				bIsMousePressed = false;
			}

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void mouseReleased(ofMouseEventArgs & e) override {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				if (!bIsOn) {
					turnOn();
				}
				else {
					turnOff();
				}
				ofNotifyEvent(buttonPressed, *this);
			}
			bIsMousePressed = false;

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		void draw() override {}

		void draw(module::Font & font) {

			ofPushMatrix();
			ofTranslate(pos.get());

			ofNoFill();
			ofSetColor(borderColor);
			ofSetLineWidth(boarderWidth);
			ofSetRectMode(OF_RECTMODE_CENTER);
			ofDrawRectRounded(0, 0, width, height, cornerRadius);
			ofSetRectMode(OF_RECTMODE_CORNER);

			/*float a = bIsOn ? 100 / 255.0 : 1.0;

			if (bIsOn) {
				ofFill();
			}
			else {
				ofNoFill();
			}

			//ofSetColor(boxColor, boxColor.get().a*a);
			ofSetColor(boxColor);
			ofDrawCircle(0, 0, radius);*/

			// draw text inside of Button
			if (bIsOn) {
				tb.draw(font);
			}
			
			ofPopMatrix();
		}

		void debugDraw(module::Font & font) {
			ofPushMatrix();
			ofTranslate(pos.get());
			tb.debugDraw(font);
			ofPopMatrix();
		}

		ofParameter<float> width{ "width", 0, 0, 1920 };
		ofParameter<float> height{ "height", 0,0,  1080 };
		ofParameter<ofColor> boxColor{ "boxColor", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		ofParameter<ofColor> borderColor{ "color", ofColor(255,255), ofColor(0,0), ofColor(255,255) };
		ofParameter<int> boarderWidth{ "width", 4, 0, 20 };
		ofParameter<int> cornerRadius{ "cornerRadius", 4, 0, 100 };
		ofParameterGroup borderPrm{ "border", borderColor, boarderWidth, cornerRadius };

		module::TextBox tb;


	private:

	};
}
