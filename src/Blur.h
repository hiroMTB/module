//
//  Blur.h
//

#pragma once

#include "ofxFboBlur.h"

namespace module{
    
    class Blur{
        
    public:
        Blur(){
            ofFbo::Settings s;
            s.width = ofGetWidth();
            s.height = ofGetHeight();
            s.internalformat = GL_RGBA;
            s.textureTarget = GL_TEXTURE_RECTANGLE_ARB;
            s.maxFilter = GL_LINEAR; GL_NEAREST;
            s.numSamples = 0;
            s.numColorbuffers = 1;
            s.useDepth = false;
            s.useStencil = false;
            blur.setup(s, false);
        }
        
        void start(){
            blur.blurOffset = offset;
            blur.blurPasses = pass;
            blur.numBlurOverlays = overlay;
            blur.blurOverlayGain = overlayGain;
            blur.gain = gain;
            blur.beginDrawScene();
        }
        
        void end(){
            blur.endDrawScene();
            blur.performBlur();
        }
        
        void draw(){
            blur.drawBlurFbo();
        }
        
        void debugDraw(){
            blur.drawSceneFBO();
        }
        
        ofParameter<float> offset{"offset", 300, 1, 1000};
        ofParameter<int> pass{"pass", 5, 1, 10};
        ofParameter<int> overlay{"overlay", 2, 1, 10};
        ofParameter<int> overlayGain{"overlay gain", 255, 0, 255};
        ofParameter<float> gain{"gain", 1, 0, 5};
        ofParameterGroup prm{"Blur", offset, pass, overlay, overlayGain, gain};

    private:
        ofxFboBlur blur;
    };
}
