//
//  ofTrueTypeFontCustom.h
//
#pragma once

#include "ofMain.h"

namespace module{
    
    class ofTrueTypeFontCustom : public ofTrueTypeFont{
        
        public:
        float getAdvance(char c) const;
        void drawStringFit(string s, float x, float y, float w, float h);
		void drawWrappedString(string s, float x, float y, float w, float h);
        
        private:
        void replaceChar(string &text, char oldChar, char newChar){
            replace_if(text.begin(), text.end(), [=](char c){return c==oldChar;}, newChar ), text.end();
        }
        
        void eraseChar(string &text, char eraseChar){
            text.erase( remove_if(text.begin(), text.end(), [=](char c){return c==eraseChar;} ), text.end() );
        }
        
        void eraseLineBreak(string &text){
            eraseChar(text, '\n');
        }
        
    };
    
}
