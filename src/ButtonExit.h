//
//  ButtonExit.h
//
#pragma once
#include "ButtonBase.h"
#include "module.h"
#include "ofxEasing.h"

using namespace ofxeasing;

namespace module{
    
    class ButtonExit : public ButtonBase{
        
    public:
        
        ButtonExit(std::string name){
            filesystem::path p = "exitButton_2x.png";
            bool ok = img.load(p.string());
            
            filesystem::path pInv = "exitButtonInv_2x.png";
            ok = imgInv.load(pInv.string());
            prm.setName(name);
        }
        
        void draw(float fadeRate=1) override{
            
            const vec2 & p = pos.get();
            
            a += bIsEnable ? 10 : -10;
            a *= fadeRate;
            a = ofClamp(a, 20, 255);
            
            ofPushStyle();
            
            if(isMousePressed()){
                //enlarge
                size += getStateChangeMillis()*0.012;
                size = ofClamp(size, sizeMin, sizeMax);
                
                if(bUsePng){
                    // added this png code because of conflict with shader
                    // but it is solved by adding ofPush/PopStyle above.
                    // just keep this code in case of future trouble
                    ofSetColor(255, a);
                    ofSetRectMode(OF_RECTMODE_CENTER);
                    imgInv.draw(p.x, p.y, size*2, size*2);
                    ofSetRectMode(OF_RECTMODE_CORNER);
                }else{
                    ofSetColor(255, a);
                    ofFill();
                    ofSetRectMode(OF_RECTMODE_CENTER);
                    ofDrawCircle(p.x, p.y, size);
                    ofSetRectMode(OF_RECTMODE_CORNER);

                    ofSetColor(0, a);
                    float d = size * sin(PI*0.25) * 0.5;
                    ofSetLineWidth(2);
                    ofDrawLine(p.x+d, p.y+d, p.x-d, p.y-d);
                    ofDrawLine(p.x-d, p.y+d, p.x+d, p.y-d);
                }
            }else{
                
                size *= 0.97;
                size = ofClamp(size, sizeMin, sizeMax);
                
                if(bUsePng){
                    ofSetColor(255, a);
                    ofSetRectMode(OF_RECTMODE_CENTER);
                    img.draw(p.x, p.y, size*2, size*2);
                    ofSetRectMode(OF_RECTMODE_CORNER);
                }else{
                    ofSetColor(255, a);
                    ofNoFill();
                    ofSetRectMode(OF_RECTMODE_CENTER);
                    ofDrawCircle(p.x, p.y, size); // x
                    ofSetRectMode(OF_RECTMODE_CORNER);
                    
                    ofSetLineWidth(2);
                    float d = size * sin(PI*0.25) * 0.5;
                    ofDrawLine(p.x+d, p.y+d, p.x-d, p.y-d);
                    ofDrawLine(p.x-d, p.y+d, p.x+d, p.y-d);
                }
                
                // pulsation
                if (pulse) {
                    ofxeasing::function f = easing(Function::Quadratic, Type::Out);
                    int frame1 = frame % end;
                    float wA = map_clamp(frame1, 0, end, 255, 0, f);
                    float wR = map_clamp(frame1, 0, end, sizeMin, sizeMax, f);

                    int frame2 = (frame + 60) % end;
                    float wA2 = map_clamp(frame2, 0, end, 255, 0, f);
                    float wR2 = map_clamp(frame2, 0, end, sizeMin, sizeMax, f);

                    ofNoFill();
                    ofSetColor(255, wA*fadeRate);
                    ofDrawCircle(p.x, p.y, wR);

                    ofSetColor(255, wA2*fadeRate);
                    ofDrawCircle(p.x, p.y, wR2);
                }
                
            }
            
            ++frame;
            
            ofPopStyle();
        }
        
    private:
        int frame{0};
        int end{120};
        
        bool bUsePng{false};
		bool pulse{false};
        ofImage img;
        ofImage imgInv;
    };
}
