//
//  Slider.h
//
#pragma once

#include "ofMain.h"
#include "module.h"
#include "ButtonBase.h"

using namespace glm;

namespace module{
    
    class Slider{
        
    public:
        
        Slider(){};
        
        void mousePressed(ofMouseEventArgs &e){
            bHit = hitTest(e.x, e.y);
            prevPos = e - vec2(ofGetWidth()/2, ofGetHeight()/2);
        }
        
        void mouseDragged(ofMouseEventArgs &e){
            if(!bHit) return;
            float mx = e.x - ofGetWidth()/2;
			float buffer = buttonSize*2;
            pos.x = ofClamp(mx, -sliderWidth/2 + buffer, sliderWidth/2 - buffer);
        }
        
        void mouseReleased(ofMouseEventArgs &e){
            if(bHit){
                // click or drag?
                vec2 mPos = e - vec2(ofGetWidth()/2, ofGetHeight()/2);
                float dist = glm::distance(mPos, prevPos);
                bDrag = dist > 10;
                
                //pos.x = bOn ? -sliderWidth : sliderWidth;
            }
            bHit = false;
        }
        
        bool hitTest(int tx, int ty){
            tx -= ofGetWidth()/2;
            ty -= ofGetHeight()/2;
            float dist = sqrt((tx-pos.x-center->x)*(tx-pos.x-center->x) + (ty-pos.y-center->y)*(ty-pos.y-center->y));
			//cout << dist << " " << buttonSize << endl;
			return dist <= (buttonSize * 2); // * 2 makes it mouse touch table friendly
        }
        
        void draw(float fade){
            
            ofPushMatrix();
            ofTranslate(center);
            
            // slider
            ofSetLineWidth(3);
            ofSetColor(255, 255*fade);
            ofDrawLine(-sliderWidth/2, 0, sliderWidth/2, 0);
            
            // button
            ofFill();
            ofSetColor(255, 255*fade);
            ofDrawCircle(pos, buttonSize);
            
            // draw text inside of Button
            int fontSize = 14;
            //ofRectangle box = module::FontManager::GothamBold[fontSize].getStringBoundingBox(label, 0, 0);
            ofSetColor(0, 255*fade);
            //module::FontManager::GothamBold[fontSize].setLetterSpacing(1.3);
            //module::FontManager::GothamBold[fontSize].drawString(label, pos.x-box.width/2.0f+2, pos.y+box.height/2.0f);
            
            ofPopMatrix();
        }
        
        void debugDraw(){
            ofSetColor(0,0,255);
            ofDrawBitmapString(ofToString(getValue(),2), center->x+pos.x-20, center->y+pos.y+20);
        }
        
        void enable(){
            reset();
            ofAddListener(ofEvents().mousePressed, this, &Slider::mousePressed);
            ofAddListener(ofEvents().mouseDragged, this, &Slider::mouseDragged);
            ofAddListener(ofEvents().mouseReleased, this, &Slider::mouseReleased);
        }
        
        void disable(){
            ofRemoveListener(ofEvents().mousePressed, this, &Slider::mousePressed);
            ofRemoveListener(ofEvents().mouseDragged, this, &Slider::mouseDragged);
            ofRemoveListener(ofEvents().mouseReleased, this, &Slider::mouseReleased);
        }
        
        // L:0.0 ~ R:1.0
        float getValue(){
			float buffer = buttonSize * 2;
			return (pos.x + (sliderWidth / 2) - buffer) / (sliderWidth - buffer * 2);
        }
        
        void reset(){
			float buffer = buttonSize * 2;
            pos = vec2(sliderWidth/2 - buffer,0);
            prevPos = pos;
        }
        
    private:
        vec2 pos;
        vec2 prevPos;
        bool bHit{false};
        bool bDrag{false};
        string label{"SLIDE"};
        
        ofParameter<vec2> center{"center", vec2(0,0),-vec2(1920/2,1080/2),vec2(1920/2,1080/2)};
        ofParameter<float> buttonSize{"buttonSize", 40, 10, 100};
        ofParameter<float> sliderWidth{"slider width", 700, 300, 1800};
        
    public:
        ofParameterGroup prm{"Slider", center, sliderWidth, buttonSize};
        
    };
}
