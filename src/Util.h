//
//  Util.h
//

#pragma once

#include "ofMain.h"
#include "ofTrueTypeFontCustom.h"

namespace module{
    
    class Util{
        
    public:
        static void drawLine( glm::vec2 p1, glm::vec2 p2, float thickness);
        static void drawLine( float x1, float y1, float x2, float y2, float thickness);
        
        static void drawCircle( float x, float y, float rad, float thickness);
        static void drawArc( float x, float y, float rad, float thickness, float startAngle, float endAngle);
        
        //static void replaceChar(string &text, char oldChar, char newChar);
        //static void eraseChar(string &text, char eraseChar);
        //static void eraseLineBreak( string & text);
        //static void stringFit(string & text, const ofTrueTypeFontCustom& font, float fitWidth);

        static filesystem::path getCommonFolder();
        
        static string replaceAll(std::string str, const std::string& from, const std::string& to);

        static int findString(string str, const string& key);
        
        static void getLoadResult(bool ok, std::string module, filesystem::path p);
    };
}
