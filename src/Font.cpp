#include "Font.h"

namespace module{
    
    void Font::load(filesystem::path _fontPath){
		fontPath = _fontPath;
        ofTrueTypeFontCustom::setGlobalDpi(72);
    }
    
    ofTrueTypeFontCustom& Font::operator[](size_t index){
		int fontSize = (int)index;
		if (!font[fontSize].isLoaded()) {
			font.insert(pair<int, ofTrueTypeFontCustom>(fontSize, ofTrueTypeFontCustom()));
			bool ok = font[fontSize].load(fontPath.string(), fontSize);
		}

        return font[(int)index];
    }
    
    Font FontManager::verdana;
	Font FontManager::ffdinBold;
	Font FontManager::ffdinRegular;
    
    void FontManager::setup(){
        filesystem::path fontPath = "verdana.ttf";
        FontManager::verdana.load(fontPath);

		filesystem::path fontPath2 = "fonts/FFDINforPUMA-Bold.ttf";
		FontManager::ffdinBold.load(fontPath2);

		filesystem::path fontPath3 = "fonts/FFDINforPUMA-Regular.ttf";
		FontManager::ffdinRegular.load(fontPath3);
    }
}
