//
//  Image.h
//
#pragma once

namespace module{
        
    class Image{
        
    public:
        
        Image(string _path){
            path = _path;
            path.addListener(this, &Image::pathChanged);
            load();
        }

		Image(string _path, string _name) {
			path = _path;
			path.addListener(this, &Image::pathChanged);
			prm.setName(_name);
			load();
		}

		
        
        Image(){
            path.addListener(this, &Image::pathChanged);
        }
        
        void draw(float fade=1, float _orientation=0){
			
			if (!show)return;

            ofPushMatrix();
            ofTranslate(pos->x, pos->y);
            ofRotateZDeg(orientation + _orientation);
            ofSetColor(color, 255*fade);
            img.draw(0,0);
            ofPopMatrix();
        }
        
        void load(){
            bool ok = img.load(path.get());
            if(ok){
                ofLogNotice() << "Load Image : " << path.get();
            }else{
                ofLogError() << "Can not load Image : " << path.get();
            }
            //assert(ok);
        }

    private:

        void pathChanged(string & p){
            if(p!=path.get()) load();
        }
        
        ofImage img;
		ofParameter<string> path{ "path", "n.a." };
		ofParameter<vec2> pos{ "position", vec2(0,0), vec2(0, 0), vec2(1920, 1080) };
        ofParameter<float> orientation{ "orientation", 0, -180, 180};
		ofParameter<bool> show{ "show", true };
		ofParameter<ofColor> color{ "color", ofColor(255, 255, 255) };

    public:
        ofParameterGroup prm{ "image", path, pos, orientation, show, color };
    };

}
