//
//  AppState.h
//
#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

namespace module{
    
    enum class AppState{
        Home    = 0,
        Story01 = 1,
        Story02 = 2,
        Story03 = 3
    };
    
    inline ostream& operator<<(ostream& os, const AppState& s){
        switch (s){
            case AppState::Home:    os << "Home  "; break;
            case AppState::Story01: os << "Story1"; break;
            case AppState::Story02: os << "Story2"; break;
            case AppState::Story03: os << "Story3"; break;
        }
        return os;
    }
    
    class AppStateMachine{
        
    public:
        
        AppStateMachine(){
            
            rules = {
                {AppState::Home,    AppState::Story01},
                {AppState::Home,    AppState::Story02},
                {AppState::Home,    AppState::Story03},
                {AppState::Story01, AppState::Home},
                {AppState::Story02, AppState::Home},
                {AppState::Story03, AppState::Home}
            };
        }
        
        ~AppStateMachine(){};
        
        bool changeState(AppState next){
            Transition p = Transition(currentState, next);
            vector<Transition>::iterator it = std::find(rules.begin(), rules.end(), p);
            if(it!=rules.end()){
                ofLogNotice("AppStateMachine") << "OK   : " << currentState << " -> " << next;
                currentState = it->second;
                return true;
            }else{
                ofLogNotice("AppStateMachine") << "WARN : " << currentState << " -> " << next;
                return false;
            }
        }
        
        AppState getCurrentState(){
            return currentState;
        }
        
    private:
        typedef pair<AppState, AppState> Transition;
        vector<Transition> rules;
        AppState currentState{ AppState::Home };
    };
    
}
