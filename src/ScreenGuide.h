#pragma once

#include "ofMain.h"

namespace module{
    
    class ScreenGuide{

    public:
        float renderW = 1920;
        float renderH = 1080;
        float centerX = renderW/2;
        float centerY = renderH/2;
        
        ofRectangle safeArea;
        
        ScreenGuide() {
            safeArea = ofRectangle(100, 100, renderW-200, renderH-200);
        }
        
        void draw(){
            ofNoFill();
            ofSetColor(0,0,255);
            ofSetLineWidth(1);
            ofDrawLine(centerX, 0, centerX, renderH);
            ofDrawLine(0, centerY, renderW, centerY);
            
            // Y=140 line
            ofDrawLine(centerX-300, 140, centerX+300, 140);
            
            //ofDrawRectangle(safeArea);
        }
    };
}
