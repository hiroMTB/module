#include "ofTrueTypeFontCustom.h"
#include "Util.h"

namespace module{
    
#define NUM_CHARACTER_TO_START 32;
    
    
    float ofTrueTypeFontCustom::getAdvance(char c) const{
        if(c==' '){
            return spaceSize;
        }
        int cy = c - NUM_CHARACTER_TO_START;
        return cps[cy].advance * letterSpacing;
    }

	//adding this to show what wrapping a string should do
	//each word is wrapped rather than each letter (as shown in drawStringFit)
	//from: https://forum.openframeworks.cc/t/text-wrapping/2953/3
	void ofTrueTypeFontCustom::drawWrappedString(string text, float x, float y, float fitWidth, float fitHeight) {
		
		if (fitWidth<0 || fitHeight<0 || text.size() <= 1) return;

		ofStringReplace(text, "\n", " ");

		string typeWrapped = "";
		string tempString = "";
		vector <string> words = ofSplitString(text, " ");

		for (int i = 0; i<words.size(); i++) {

			string wrd = words[i];

			// if we aren't on the first word, add a space  
			if (i > 0) {
				tempString += " ";
			}
			tempString += wrd;

			ofRectangle r = getStringBoundingBox(tempString, 0, 0);

			if (r.width >= fitWidth) {
				typeWrapped += "\n";
				tempString = wrd;// make sure we're including the extra word on the next line  
			}
			else if (i > 0) {
				// if we aren't on the first word, add a space  
				typeWrapped += " ";
			}

			typeWrapped += wrd;
		}

		if (fitHeight == 0) {
			drawString(typeWrapped, x, y);
		}
		else {
			string finalText = "";

			// remove string which overs fitHeight
			vector<string> lines = ofSplitString(typeWrapped, "\n");
			int heightSum = 0;
			for (int i = 0; i<lines.size(); i++) {
				ofRectangle r = getStringBoundingBox(lines[i], 0, 0);
				float h = r.height;
				if (heightSum + h <= fitHeight) {
					if (i != 0) finalText += "\n";
					finalText += lines[i];
					heightSum += lineHeight;
				}
				else {
					break;
				}
			}

			drawString(finalText, x, y);
		}
	}
    
    void ofTrueTypeFontCustom::drawStringFit(string text, float x, float y, float fitWidth, float fitHeight){
        
        if(fitWidth<0 || fitHeight<0 || text.size()<=1) return;
        
        int directionX = settings.direction == OF_TTF_LEFT_TO_RIGHT?1:-1;
        int lineHeight = getLineHeight();
        
        eraseLineBreak(text);

        // fisrt we limit width
        const size_t TAB_WIDTH = 4; /// Number of spaces per tab
        uint32_t prevC = 0;
        int widthSum = 0;
        int index = 0;
        vector<int> breakPosition;
        iterateString(text, x, y, false, [&](uint32_t c, glm::vec2 pos){

            float w = 0;
            
            if (c == '\n') {
                widthSum = 0;
                prevC = 0;
            }else if (c == '\t') {
                w = getGlyphProperties(' ').advance * TAB_WIDTH * letterSpacing  * directionX;
                prevC = c;
            } else if(isValidGlyph(c)) {
                
                const auto & props = getGlyphProperties(c);
                if(prevC>0){
                    w += getKerning(c,prevC);// * directionX;
                }
                
                w += props.advance * letterSpacing * directionX;
            }
                
            widthSum += w;
            //cout << "charactor " << c-48 << ", width = " << w << ", totalW = " << widthSum << endl;
            
            if(widthSum >= fitWidth){
                widthSum = w;
                prevC = 0;
                breakPosition.push_back(index);
            }else{
                prevC = c;
            }
            
            index += 1;
        });
        
        // insert line break
        for(int i=0; i<breakPosition.size(); i++){
            int insertPos = breakPosition[i]+i;
            if(insertPos<text.size()){
                text.insert(insertPos, "\n");
            }
        }
        
        if(fitHeight==0){
            drawString(text, x, y);
        }else{
            string finalText = "";
            
            // remove string which overs fitHeight
            vector<string> lines = ofSplitString(text, "\n");
            int heightSum = 0;
            for (int i=0; i<lines.size(); i++){
                ofRectangle r = getStringBoundingBox(lines[i], 0, 0);
                float h = r.height;
                if(heightSum+h <= fitHeight){
                    if(i!=0) finalText += "\n";
                    finalText += lines[i];
                    heightSum += lineHeight;
                }else{
                    break;
                }
            }
            
            drawString(finalText, x, y);
        }
    }    
}
