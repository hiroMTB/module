//
//  ButtonPulse.h
//
#pragma once
#include "ButtonBase.h"
#include "ofxEasing.h"

using namespace ofxeasing;

namespace module{
    
    class ButtonPulse : public ButtonBase{
        
    public:
        
        void draw(float fade) override{
            
            if(isMousePressed()){
                //enlarge
                size += getStateChangeMillis()*0.05;
                size = ofClamp(size, sizeMin, sizeMax);
                ofSetColor(255, 255*fade);
                ofNoFill();
                ofDrawCircle(x, y, size);
            }else{
                
                size *= 0.97;
                size = ofClamp(size, sizeMin, sizeMax);
                
                // core
                ofSetColor(255, 255*fade);
                ofFill();
                ofDrawCircle(x, y, size);
                
                // pulsation
                ofxeasing::function f = easing(Function::Quadratic, Type::Out);
                int frame1 = frame % end;
                float wA = map_clamp(frame1, 0, end, 255, 0, f);
                float wR = map_clamp(frame1, 0, end, sizeMin, sizeMax, f);

                int frame2 = (frame+60) % end;
                float wA2 = map_clamp(frame2, 0, end, 255, 0, f);
                float wR2 = map_clamp(frame2, 0, end, sizeMin, sizeMax, f);
                
                ofNoFill();
                ofSetColor(255, wA*fade);
                ofDrawCircle(x, y, wR);
                
                ofSetColor(255, wA2*fade);
                ofDrawCircle(x, y, wR2);
            }
            
            ++frame;
        }
    
    private:
        int frame{0};
        int end{120};
    };
}
