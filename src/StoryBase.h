//
//  StoryBase.h
//
#pragma once
#include "ofMain.h"
#include "ofxGui.h"
#include "module.h"

using namespace std;

namespace module{
    
    class StoryBase{
        
    public:
        
        StoryBase(){
            time.setSerializable(false);
            bPause.setSerializable(false);
            duration.addListener(this, &StoryBase::durationChanged);
        }
        
        StoryBase(StoryBase& x)=default;
        template<class T> StoryBase(T& x)=delete;
        StoryBase & operator=(StoryBase const &)=default;
        template<class T> StoryBase & operator=(T const &)=delete;
        virtual ~StoryBase(){};
        
		virtual void setup() {};
		virtual void update() {};
		virtual void draw() {};
		virtual void drawLeft() {};
		virtual void drawCentre() {};
		virtual void drawRight() {};
        
		virtual void debugDraw() {};

        virtual void enable(){};
        virtual void disable(){};
        virtual void reset(){};
        virtual void switchLanguage(bool fr) = 0;

        bool updateTime(bool resetOnMousePressed=true){

            if (resetOnMousePressed && ofGetMousePressed()) {
                resetTime();
            }
            
            if(!bPause){
				frame += 1.0;
                time = frame/ofGetTargetFrameRate();
            }
            return frame < floor(duration * ofGetTargetFrameRate());
        }

		void updateGlobalFade() {
			globalFade = ofClamp(globalFade + 0.05, 0.0f, 1.0f);
		}

		bool isGlobalFadeComplete() {
			return globalFade >= 1.0f;
		}

		void resetTime() {
			frame = 0;
		}
        
        virtual void drawGui(){
            gui.draw();
        }
        
        void durationChanged(float & f){
            time.setMax(f);
        }
        
        template<class ListenerClass, typename ListenerMethod>
        void addListener(ListenerClass * listener, ListenerMethod method, int prio=OF_EVENT_ORDER_AFTER_APP){
            ofAddListener(endStoryEvent, listener, method);
        }
        
        template<class ListenerClass, typename ListenerMethod>
        void removeListener(ListenerClass * listener, ListenerMethod method, int prio=OF_EVENT_ORDER_AFTER_APP){
            ofRemoveListener(endStoryEvent, listener, method);
        }
        
        ofxPanel gui;
        ofEvent<void> endStoryEvent;
		float globalFade{ 1 };
    
        // internaly we use frame for animation control
        // time is just for dispaly
        ofParameter<bool> bPause{"Pause", false};
        ofParameter<int> time{"time (sec)", 0, 0, 30};
        ofParameter<float> duration{"duration (sec)", 10, 0, 120};
        ofParameterGroup timePrm{"timeline", bPause, time, duration};

    private:
        int frame{0};
    };
    
}
