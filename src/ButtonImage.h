//
//  ButtonImage.h
//
#pragma once
#include "module.h"
#include "ButtonBase.h"

namespace module {
    class ButtonImage : public module::ButtonBase {
        
    public:
        
        ButtonImage(string name, filesystem::path p){
            bool ok = img.load(p.string());
			width = img.getWidth();
			height = img.getHeight();
            prm.setName(name);
            prm.add(width);
            prm.add(height);
        }

		bool hitTest(int tx, int ty) const override{
			const vec2 &p = pos.get();
			return ((tx > p.x - width/2) && (tx < p.x + width/2) && (ty > p.y - height/2) && (ty < p.y + height/2));
		}
        
        void draw() override {
            const vec2 &p = pos.get();
            float a = isMousePressed() ? 100 : 255;
            ofSetRectMode(OF_RECTMODE_CENTER);
            ofSetColor(255, a);
            img.draw(p.x, p.y, width, height);
            ofSetRectMode(OF_RECTMODE_CORNER);
        }
        
        ofParameter<float> width{ "width", 0, 0, 1920 };
        ofParameter<float> height{ "height", 0,0,  1080 };

	private:
        ofImage img;     
    };
}
