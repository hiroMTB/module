//
//  Button.h
//
#pragma once

#include "ofMain.h"
#include "Util.h"
#include <unordered_map>
#include "ofTrueTypeFontCustom.h"

namespace module{
    
    class Font{
        
    public:        
        void load(filesystem::path fontPath);      
        ofTrueTypeFontCustom& operator[](size_t index);

        unordered_map<int, ofTrueTypeFontCustom> font;
		filesystem::path fontPath;
    };
    
    
    class FontManager{
        
    public:

        static Font verdana;
		static Font ffdinBold;
		static Font ffdinRegular;

        static void setup();

    };
}
