//
//  ToggleText.h
//

#pragma once
#include "module.h"
#include "ButtonBase.h"

namespace bs {
	class ToggleText : public module::ButtonBase {

	public:

		ToggleText(string _text){
            text = _text;
        }

		void mouseReleased(int x, int y, int button)override {
			if (hitTest(x, y)) {
				bOn = !bOn;
			}
		}

		bool hitTest(int tx, int ty) {
			tx -= ofGetWidth() / 2;
			ty -= ofGetHeight() / 2;
			float dist = sqrt((tx - x)*(tx - x) + (ty - y)*(ty - y));
			return dist <= (size * 3); // * 2 makes it mouse touch table friendly
		}
        
		void draw(float fadeRate) override {

			if (bOn) {
				size += getStateChangeMillis()*0.012;
			}
			else {
				size *= 0.97;
			}

			size = ofClamp(size, sizeMin, sizeMax);

			ofFill();
			ofSetColor(255, 255 * fadeRate);
			ofDrawCircle(x, y, size);
  
            // draw text inside of Button
            int fontSize = 14;
            //ofRectangle box = module::FontManager::GothamBold[fontSize].getStringBoundingBox(text, 0, 0);
            ofSetColor(0, 255 * fadeRate);
            //module::FontManager::GothamBold[fontSize].setLetterSpacing(1.3);
            //module::FontManager::GothamBold[fontSize].drawString(text, x - box.width / 2.0f + 3, y + box.height / 2.0f + 2);
            
		}

        void reset() override{
            bOn = false;
            size = sizeMin;
        }
        
		string text{ "" };
		ofParameter<bool> bOn{ false };
	};
}
