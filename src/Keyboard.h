#pragma once

#include "ofMain.h"

using namespace glm;

namespace module {
    //--------------------------------------------------------------
    class keyboardKey {
    public:
        int ID;
        int x;
        int y;
        int w;
        int h;
        string s;
        string sAlt;
		string sAlt_fr;
        
        //--------------------------------------------------------------
        bool hitTest(int tx, int ty) {
            return ((tx > x) && (tx < x + w) && (ty > y) && (ty < y + h));
        }
        
    };
    
    class Keyboard  {
        
    public:
        
        //--------------------------------------------------------------
        void setup(int _x, int _y, string fontFilePath, string jsonFilePath="keys.json") {
            
            backIcon.load("images/backIcon.png");
            backIcon.setAnchorPercent(0.5, 0.5);
            
            font.load(fontFilePath, 28);
            
			int w = 0;
			int h = 0;

            json = ofLoadJson(jsonFilePath);
            for (int i=0; i<json.size(); i++) {
                
                keyboardKey k;
                int kx = json[i]["x"];
                int ky = json[i]["y"];
                k.ID = i;
                k.x = kx;
                k.y = ky;
                k.w = json[i]["w"];
                k.h = json[i]["h"];
                k.s = json[i]["s"];
                k.sAlt = json[i]["sAlt"];
				k.sAlt_fr = json[i]["sAlt_fr"];
                keys.push_back(k);

				//width and height
				if (k.x + k.w > w) w = k.x + k.w;
				if (k.y + k.h > h) h = k.y + k.h;
            }
            
			width = w;
			height = h;

			disable();
        }
        
        //--------------------------------------------------------------
        void draw() {

			

			ofPushMatrix();
			ofTranslate(pos.get().x - width/2, pos.get().y - height/2);

            if(currKey != -1){
				string s = keys[currKey].s;
				if (s != "DONE" || bShowDoneBtn) {
					ofSetColor(fillColor.get().r, fillColor.get().g, fillColor.get().b, 100);
					ofDrawRectRounded(keys[currKey].x, keys[currKey].y, keys[currKey].w, keys[currKey].h, keys[currKey].h / 2);
				}
            }
            
			
            
            
            ofSetLineWidth(boarderWidth);
            for(auto k : keys){

				ofFill();
				ofSetColor(bgColor);
				ofDrawRectRounded(k.x, k.y, k.w, k.h, k.h / 2);

				ofNoFill();
				ofSetColor(borderColor);
                ofDrawRectRounded(k.x, k.y, k.w, k.h, k.h/2);
                
				ofSetColor(fillColor);
                if(k.s == "BACK"){
                    backIcon.draw(k.x + (k.w/2), k.y + (k.h/2));
                }else if(k.s == "ALT"){

                    string s = "123";
                    if(alt){
                        s = "ABC";
                    }
                    float sw = font.stringWidth(s);
                    float sh = font.stringHeight(s);
                    font.drawString(s, k.x + (k.w/2) - (sw/2), k.y + (k.h/2) + (sh/2));
                }
                else if(!alt){

					string s = k.s;

                    float sw = font.stringWidth(s);
                    float sh = font.stringHeight(s);
                    font.drawString(s, k.x + (k.w/2) - (sw/2), k.y + (k.h/2) + (sh/2));
                }else{

					string s = bFrench ? k.sAlt_fr : k.sAlt;

                    float sw = font.stringWidth(s);
                    float sh = font.stringHeight(s);
                    font.drawString(s, k.x + (k.w/2) - (sw/2), k.y + (k.h/2) + (sh/2));
                }

				//draw overlay to signal done button is disabled
				if (k.s == "DONE" && !bShowDoneBtn) {
					ofFill();
					ofSetColor(0, 0, 0, 100);
					ofDrawRectRounded(k.x, k.y, k.w, k.h, k.h / 2);
				}
            }
            ofSetLineWidth(1);
            ofFill();
			
			

			ofPopMatrix();
        }
        
        //--------------------------------------------------------------
        void reset(){
            keysPressed.clear();
            currKey = -1;
        }
        
        void enable(){
			bIsEnable = true;
			ofAddListener(ofEvents().mousePressed, this, &Keyboard::mousePressed);
			ofAddListener(ofEvents().mouseMoved, this, &Keyboard::mouseMoved);
			ofAddListener(ofEvents().mouseDragged, this, &Keyboard::mouseDragged);
			ofAddListener(ofEvents().mouseReleased, this, &Keyboard::mouseReleased);
        }
        
        void disable(){
			bIsEnable = false;
			ofRemoveListener(ofEvents().mousePressed, this, &Keyboard::mousePressed);
			ofRemoveListener(ofEvents().mouseMoved, this, &Keyboard::mouseMoved);
			ofRemoveListener(ofEvents().mouseDragged, this, &Keyboard::mouseDragged);
			ofRemoveListener(ofEvents().mouseReleased, this, &Keyboard::mouseReleased);
        }

		bool hitTest(int tx, int ty) {
			const vec2 &p = pos.get();
			return ((tx > p.x - width/2) && (tx < p.x + width/2) && (ty > p.y - height/2) && (ty < p.y + height/2));
		}

		void mouseMoved(ofMouseEventArgs &e) {
			if (!bIsEnable) return;
		}

		void mousePressed(ofMouseEventArgs &e) {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
				float x = e.x - (pos.get().x - width/2);
				float y = e.y - (pos.get().y - height/2);

				for (int i = 0; i<keys.size(); i++) {
					if (keys[i].hitTest(x, y)) {
						currKey = i;
					}
				}
			}
			else {
				bIsMousePressed = false;
			}
		}

		void mouseDragged(ofMouseEventArgs &e) {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
				float x = e.x - (pos.get().x - width / 2);
				float y = e.y - (pos.get().y - height / 2);

				for (int i = 0; i<keys.size(); i++) {
					if (keys[i].hitTest(x, y)) {
						currKey = i;
					}
				}
			}
			else {
				bIsMousePressed = false;
				currKey = -1;
			}
		}

		void mouseReleased(ofMouseEventArgs &e) {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				float x = e.x - (pos.get().x - width / 2);
				float y = e.y - (pos.get().y - height / 2);

				if (currKey == -1) return;

				getText(x, y);

			}
			currKey = -1;
			bIsMousePressed = false;

		}

		void getText(int x, int y) {
			for (int i = 0; i<keys.size(); i++) {

				if (keys[i].hitTest(x, y) && keys[i].ID == keys[currKey].ID) {

					string tmp;
					string key = (alt == false ? keys[i].s : (bFrench ? keys[i].sAlt_fr : keys[i].sAlt));
					if (key == "BACK") {

						//remove
						if (keysPressed.size() == 0) return;

						keysPressed.pop_back();

						for (auto e : keysPressed) {
							tmp += e;
						}

					}
					else if (key == "ALT") {

						alt = !alt;
						currKey = -1;
						return;

					}
					else if (key == "DONE") {
						
						if (!bShowDoneBtn) return;

						bool done = true;
						ofNotifyEvent(donePressed, done);
						currKey = -1;
						return;

					}
					else {

						for (auto e : keysPressed) {
							tmp += e;
						}

						if (tmp.size() + key.size() > 30) return;



						keysPressed.push_back(key);

						tmp += key;
					}

					ofNotifyEvent(keyPressed, tmp);
				}
			}
		}

		void removeLastLetter() {
			//remove
			if (keysPressed.size() == 0) return;

			keysPressed.pop_back();
		}
        
        
        ofEvent<string> keyPressed;
        ofEvent<bool> donePressed;

		ofParameter<vec2> pos{ "position", vec2(0,0), vec2(0,0), vec2(1920,1080) };
		ofParameter<float> width{ "width", 0, 0, 1920 };
		ofParameter<float> height{ "height", 0,0,  1080 };

		ofParameter<ofColor> borderColor{ "color", ofColor(255,255), ofColor(0,0), ofColor(255,255) };
		ofParameter<int> boarderWidth{ "width", 4, 0, 20 };
		ofParameterGroup borderPrm{ "border", borderColor, boarderWidth };

		ofParameter<ofColor> bgColor{ "bgColor", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		ofParameter<ofColor> fillColor{ "fill", ofColor(255,0), ofColor(0,0), ofColor(255,255) };
		ofParameterGroup prm{ "keyboard", pos, width, height, borderPrm, fillColor, bgColor };

		bool bShowDoneBtn{ true };
		bool bFrench{ false };

    private:
        ofJson json;
        ofImage backIcon;
        vector<keyboardKey> keys;
        vector<string> keysPressed;
		int currKey{ -1 };
		bool alt{ false };
		bool bIsMousePressed;
		bool bIsEnable;
        ofTrueTypeFont font;
        
        
    };
};


