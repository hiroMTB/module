//
//  ButtonBase.h
//
#pragma once
#include "ofMain.h"
#include "module.h"

using namespace glm;

namespace module{
    
    class ButtonBase{
        
    public:
        
        ButtonBase(){
			stateChangeTimestampMillis = 0;
            enable();
        }
                
        virtual void draw() = 0;
        
        virtual bool hitTest(int tx, int ty) const = 0;

		bool isMousePressed() {
			return bIsMousePressed;
		}

		unsigned long getStateChangeMillis() const {
			return ofGetElapsedTimeMillis() - stateChangeTimestampMillis;
		}

		virtual void mouseMoved(ofMouseEventArgs &e) {
			if (!bIsEnable) return;

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		virtual void mousePressed(ofMouseEventArgs &e) {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
			}
			else {
				bIsMousePressed = false;
			}

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		virtual void mouseDragged(ofMouseEventArgs &e) {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				bIsMousePressed = true;
			}
			else {
				bIsMousePressed = false;
			}

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}

		virtual void mouseReleased(ofMouseEventArgs &e) {
			if (!bIsEnable) {
				bIsMousePressed = false;
				return;
			}

			if (hitTest(e.x, e.y)) {
				ofNotifyEvent(buttonPressed, *this);
			}
			bIsMousePressed = false;

			stateChangeTimestampMillis = ofGetElapsedTimeMillis();
		}
        
        template<class ListenerClass, typename ListenerMethod>
        void addListener(ListenerClass * listener, ListenerMethod method, int prio=OF_EVENT_ORDER_AFTER_APP){
            ofAddListener(buttonPressed, listener, method);
        }
        
        template<class ListenerClass, typename ListenerMethod>
        void removeListener(ListenerClass * listener, ListenerMethod method, int prio=OF_EVENT_ORDER_AFTER_APP){
            ofRemoveListener(buttonPressed, listener, method);
        }
        
        virtual void reset(){};
            
        void enable(){
            bIsEnable = true;
			ofAddListener(ofEvents().mousePressed, this, &ButtonBase::mousePressed);
			ofAddListener(ofEvents().mouseMoved, this, &ButtonBase::mouseMoved);
			ofAddListener(ofEvents().mouseDragged, this, &ButtonBase::mouseDragged);
			ofAddListener(ofEvents().mouseReleased, this, &ButtonBase::mouseReleased);
            reset();
        }
        
        void disable(){
            bIsEnable = false;
			ofRemoveListener(ofEvents().mousePressed, this, &ButtonBase::mousePressed);
			ofRemoveListener(ofEvents().mouseMoved, this, &ButtonBase::mouseMoved);
			ofRemoveListener(ofEvents().mouseDragged, this, &ButtonBase::mouseDragged);
			ofRemoveListener(ofEvents().mouseReleased, this, &ButtonBase::mouseReleased);
        }
       
        virtual void turnOn(){
            bIsOn = true;
        }
        
		virtual void turnOff(){
            bIsOn = false;
        }
        
        bool getIsOn(){
            return bIsOn;
        }
        
		ofParameter<vec2> pos{ "position", vec2(0,0), vec2(0,0), vec2(1920,1080) };
        ofParameterGroup prm{ "buton", pos};

    public:
        float a{ 255 };
		ofEvent<module::ButtonBase> buttonPressed;

		bool bIsEnable;
		bool bIsMousePressed;
		float stateChangeTimestampMillis;

    protected:
        bool bIsOn{ false };
        
    };
}
