#pragma once
#include "ofMain.h"
#include "ofxTuioClient.h"

namespace module{

	class TuioToMouse {

	public:

		void setup(int port) {
			cout << "connect" << endl;
			tuioClient.connect(port);

			ofAddListener(ofEvents().touchDown, this, &TuioToMouse::touchDown);
			ofAddListener(ofEvents().touchUp, this, &TuioToMouse::touchUp);
			ofAddListener(ofEvents().touchMoved, this, &TuioToMouse::touchMoved);

			ofAddListener(ofEvents().update, this, &TuioToMouse::update);

		}

		void update(ofEventArgs &args) {
			tuioClient.update();
		}

		void touchDown(ofTouchEventArgs & touch) {
			ofEvents().notifyMousePressed(touch.x * ofGetWidth(), touch.y * ofGetHeight(), 0);
		}

		void touchUp(ofTouchEventArgs & touch) {
			ofEvents().notifyMouseReleased(touch.x * ofGetWidth(), touch.y * ofGetHeight(), 0);
		}

		void touchMoved(ofTouchEventArgs & touch) {
			ofEvents().notifyMouseDragged(touch.x * ofGetWidth(), touch.y * ofGetHeight(), 0);
		}

		ofxTuioClient tuioClient;

	};
}
