#pragma once

#include "ofMain.h"
#include "module.h"
#include "ofxGui.h"

using namespace module;

class ofApp : public ofBaseApp{
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    

    TextBox textBox1;
    TextBox textBox2;
    ofxPanel gui;
};
