#include "ofApp.h"

void ofApp::setup(){

    ofSetWindowShape(1920, 1080);
    ofSetWindowPosition(0, 0);
    
    textBox1.prm.setName("myTextBox 1");
    textBox2.prm.setName("myTextBox 2");

    gui.setup("settings", "settings.json");
    gui.add(textBox1.prm);
    gui.add(textBox2.prm);
    gui.loadFromFile("settings.json");
    FontManager::setup();
    
}

void ofApp::update(){

}

void ofApp::draw(){

    ofSetupScreenOrtho();
    ofPushMatrix();
    {
        ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
        
        ofSetColor(0);
        ofDrawLine(-50, 0, 50, 0);
        ofDrawLine(0, -50, 0, 50);
        
        ofSetColor(0,0,255);
        textBox1.draw(module::FontManager::adineuePRO);
        textBox1.debugDraw(module::FontManager::adineuePRO);

        textBox2.draw(module::FontManager::barlowCondensed);
        textBox2.debugDraw(module::FontManager::barlowCondensed);
    }
    ofPopMatrix();
    
    gui.draw();
}

void ofApp::keyPressed(int key){

    switch(key){
            
    }
}
